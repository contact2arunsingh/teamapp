*********************************************
## INSTALLATION INSTRUCTIONS FOR WINDOWS ##
*********************************************
https://docs.google.com/document/d/1y1XHeaKYfyOg7mzP8zSYy0Y_4awx4wnP4fIl_Ja3yqs/edit

1) Copy "ProjectOB" folder to "Documents"
2) Open terminal and Check for java by writing command "java -version"
3) If Java is not installed download and install from "https://java.com/en/download/"
4) Double click on "projectOBScheduler.jar". It will show an alert
that entry has been done in registry
5) Reboot the PC
6) Right click on "login.jar" and choose "create shortcut and send to desktop"
7) Hide the folder created in step 3 above
8) Goto dashboard and right click on "login.jar" shortcut icon.
Choose change icon and select the projectob.ico path present in same folder where 
both jar files are.
9) Open windows start up folder and paste a copy of shortcut here too.
10) Restart the PC

*********************
TROUBLESHOOT::
*********************
For windows if projectOB was already present you may need to remove entry from registry before
proceeding for new install. To access the registry
1) Open command prompt
2) Run "regedit"
3) Within registry window
Current users > Softwares > Microsoft >
Windows > Current version > run > projectob
(Right click and delete the existing entry)

If troubleshooting required in login.jar open command prompt and run the build as 
java -jar /va/path of login.jar

If troubleshooting required in projectOBScheduler.jar open command prompt and run the build as 
java -jar /va/path of projectOBScheduler.jar

******
Note:
******
1) If jar show errors in windows /Temp/ folder for dll load.
Simply delete the temp folder

2) If jar show bind error that means 2 instance of login.jar has been executed.
Solution is to reboot the pc.


